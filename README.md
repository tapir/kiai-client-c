Sample Bot
----------

This bot moves to the the first empty cell and then looks for enemies. If it can find an enemy it attacks otherwise it attacks in a random direction.

```C
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "kiaic.h"

int main() {
	// Only needed for mintty show printf without buffering
	setvbuf(stdout, 0, _IONBF, 0);
	
	// Initialize random number generator
	srand(time(NULL));
	
	// TestBot version 1.0
	KIAI_GameConfig c = KIAI_Connect("127.0.0.1", "TestBot", 1, 0);
	
	// Print server configs
	fprintf(stdout, "Board width: %d\n", c.Width);
	fprintf(stdout, "Board height: %d\n", c.Height);
	fprintf(stdout, "Maximum moves per turn: %d\n", c.MaxMoves);
	fprintf(stdout, "Maximum fires per turn: %d\n", c.MaxFires);
	fprintf(stdout, "Initial Health: %d\n", c.MaxHealth);
	fprintf(stdout, "Number of bots: %d\n", c.PlayerCount);
	fprintf(stdout, "Turn timeout: %d\n\n", c.Timeout);
	
	// Main loop
	while(true) {
		// Bot waits for its turn
		KIAI_TurnStatus t = KIAI_WaitForTurn();
		
		// Move to the first random empty cell
		while(true) {
			int direction = rand() % 8;
			if (t.Radar[direction] == 0) {
				KIAI_Move(&t, direction);
				break;
			}
		}
		
		// Fire if there is anybody around
		for(int i = 0; i < 8; i++) {
			if(t.Radar[i] > 1) {
				KIAI_Fire(&t, i);
				break;
			}
		}
		
		// Or fire randomly if nobody's around
		if(t.FiresLeft > 0) {
			KIAI_Fire(&t, rand() % 8);
		}
		
		// End turn
		KIAI_EndTurn();
	}
	
	KIAI_Disconnect();
}
```
