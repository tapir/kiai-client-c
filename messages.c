#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "messages.h"

enum {
	playerDataID = 1,
	moveID = 3,
	fireID = 4,
	endTurnID = 5
};

void encodePlayerData(uint8_t *output, char *playerName, uint8_t playerVersionMajor, uint8_t playerVersionMinor) {
	// 1  byte for message type
	// 16 bytes for robotName
	// 1  byte for robotVersionMajor
	// 1  byte for robotVersionMinor
	
	memset(output, 0, 19);
	
	// Set msg ID
	output[0] = (uint8_t) playerDataID;
	
	// Copy player name
	int n = strlen(playerName);
	n = n > 15 ? 15 : n;
	strncpy(&output[1], playerName, n);
	
	// Copy playerVersions
	output[17] = playerVersionMajor;
	output[18] = playerVersionMinor;
}

void encodeMove(uint8_t *output, uint8_t d) {
	// 1 byte for message type
	// 1 byte for direction
	
	output[0] = moveID;
	output[1] = d;
}

void encodeFire(uint8_t *output, uint8_t d) {
	// 1 byte for message type
	// 1 byte for direction
	
	output[0] = fireID;
	output[1] = d;
}

void encodeEndTurn(uint8_t *output) {
	// 1 byte for message type
	// 1 byte for direction
	
	output[0] = endTurnID;
}

KIAI_TurnStatus decodeNewTurn(uint8_t *input) {
	// 1 byte for X
	// 1 byte for Y
	// 1 byte for health
	// 1 byte for moves left
	// 1 byte for fires left
	// 1 byte for players left
	// 8 bytes for radar
	
	KIAI_TurnStatus t = {	
		.X = input[1],
		.Y = input[2],
		.Health = input[3],
		.MovesLeft = input[4],
		.FiresLeft = input[5],
		.PlayersLeft = input[6],
	};
	memcpy(t.Radar, &input[7], 8);

	return t;
}

_moveStatus decodeMove(uint8_t *input) {
	// 1 byte for X
	// 1 byte for Y
	// 1 byte for moves left
	// 8 bytes for radar
	
	_moveStatus m = {
		.X = input[1],
		.Y = input[2],
		.MovesLeft = input[3],
	};
	memcpy(m.Radar, &input[4], 8);
	
	return m;
}

_fireStatus decodeFire(uint8_t *input) {
	// 1 byte for players left
	// 1 byte for fires left
	// 8 bytes for radar
	
	_fireStatus f = {
		input[1],
		input[2],
	};
	memcpy(f.Radar, &input[3], 8);
	
	return f;
}

KIAI_GameConfig decodeGameConfig(uint8_t *input) {
	// 1 byte for board width
	// 1 byte for board height
	// 1 byte for max moves allowed per turn
	// 1 byte for max fires alowed per turn
	// 1 byte for max health
	// 1 byte for number of players
	// 1 byte for turn timeout
	
	KIAI_GameConfig g = {
		input[1],
		input[2],
		input[3],
		input[4],
		input[5],
		input[6],
		input[7],
	};
	
	return g;
}
