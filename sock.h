#ifdef _WIN32
	#ifndef _WIN32_WINNT
		#define _WIN32_WINNT 0x0501
	#endif
	#include <winsock2.h>
	#include <Ws2tcpip.h>
#else
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <netdb.h>
	#include <unistd.h>
	#include <netinet/tcp.h>
	typedef int SOCKET;
#endif

int kiai_read(SOCKET sock, void *buf, unsigned int n) {
	#ifdef _WIN32
		return recv(sock, buf, n, 0);
	#else
		return read(sock, buf, n);
	#endif
}

int kiai_write(SOCKET sock, void *buf, unsigned int n) {
	#ifdef _WIN32
		return send(sock, buf, n, 0);
	#else
		return write(sock, buf, n);
	#endif
}

int sock_init() {
	#ifdef _WIN32
		WSADATA wsa_data;
		return WSAStartup(MAKEWORD(1, 1), &wsa_data);
	#else
		return 0;
	#endif
}

int sock_quit() {
	#ifdef _WIN32
		return WSACleanup();
	#else
		return 0;
	#endif
}

int sock_close(SOCKET sock) {
	int status = 0;

	#ifdef _WIN32
		status = shutdown(sock, SD_BOTH);
		if (status == 0) { status = closesocket(sock); }
	#else
		status = shutdown(sock, SHUT_RDWR);
		if (status == 0) { status = close(sock); }
	#endif

	return status;
}
