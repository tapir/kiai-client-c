#ifndef _KIAIC_H_
#define _KIAIC_H_

#include <stdint.h>

enum {
	KIAI_NorthWest,
	KIAI_North,
	KIAI_NorthEast,
	KIAI_East,
	KIAI_SouthEast,
	KIAI_South,
	KIAI_SouthWest,
	KIAI_West
};

typedef struct {
	uint8_t	X;
	uint8_t	Y;
	uint8_t Health;
	uint8_t MovesLeft;
	uint8_t FiresLeft;
	uint8_t PlayersLeft;
	uint8_t Radar[8];
} KIAI_TurnStatus;

typedef struct {
	uint8_t Width;
	uint8_t Height;
	uint8_t MaxMoves;
	uint8_t MaxFires;
	uint8_t MaxHealth;
	uint8_t PlayerCount;
	uint8_t Timeout;
} KIAI_GameConfig;

KIAI_GameConfig KIAI_Connect(char *server, char *playerName, uint8_t playerVersionMajor, uint8_t playerVersionMinor);
KIAI_TurnStatus KIAI_WaitForTurn();
void KIAI_Move(KIAI_TurnStatus *s, uint8_t direction);
void KIAI_Fire(KIAI_TurnStatus *s, uint8_t direction);
void KIAI_EndTurn();
void KIAI_Disconnect();

#endif
