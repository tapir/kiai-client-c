#include <stdio.h>
#include <assert.h>

#include "client.h"
#include "sock.h"
#include "messages.h"

static SOCKET sock;

void isConnAlive(int n) {
	if(n <= 0) {
		fprintf(stdout, "[CLIENT] Connection closed.\n");
		fprintf(stdout, "[CLIENT] Quitting...\n");
		sock_close(sock);
		sock_quit();
		exit(1);
	}
}

KIAI_GameConfig KIAI_Connect(char *server, char *playerName, uint8_t playerVersionMajor, uint8_t playerVersionMinor) {
	sock_init();
	struct sockaddr_in serv;
	uint8_t sendBuffer[19];
	uint8_t receiveBuffer[32];
	char flag = 1;
	
	sock = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(flag));
	
	serv.sin_family	= AF_INET;
	serv.sin_port = htons(4000);
	serv.sin_addr.s_addr = inet_addr(server);
	
	if(connect(sock, (struct sockaddr *)&serv, sizeof(serv)) < 0) {
		fprintf(stdout, "[CLIENT] Can't connect to remote host %s\n", server);
		fprintf(stdout, "[CLIENT] Quitting...\n");
		exit(1);
	}
	
	encodePlayerData(sendBuffer, playerName, playerVersionMajor, playerVersionMinor);
	isConnAlive(kiai_write(sock, sendBuffer, sizeof(sendBuffer)));
	
	isConnAlive(kiai_read(sock, receiveBuffer, sizeof(receiveBuffer)));
	return decodeGameConfig(receiveBuffer);
}

KIAI_TurnStatus KIAI_WaitForTurn() {
	uint8_t receiveBuffer[32];
	isConnAlive(kiai_read(sock, receiveBuffer, sizeof(receiveBuffer)));
	return decodeNewTurn(receiveBuffer);
}

void KIAI_Move(KIAI_TurnStatus *s, uint8_t direction) {
	uint8_t sendBuffer[2];
	uint8_t receiveBuffer[32];
	
	encodeMove(sendBuffer, direction);
	isConnAlive(kiai_write(sock, sendBuffer, sizeof(sendBuffer)));
	
	isConnAlive(kiai_read(sock, receiveBuffer, sizeof(receiveBuffer)));
	_moveStatus m = decodeMove(receiveBuffer);
	
	s->X = m.X;
	s->Y = m.Y;
	s->MovesLeft = m.MovesLeft;
	memcpy(s->Radar, m.Radar, 8);
}

void KIAI_Fire(KIAI_TurnStatus *s, uint8_t direction) {
	uint8_t sendBuffer[2];
	uint8_t receiveBuffer[32];
	
	encodeFire(sendBuffer, direction);
	isConnAlive(kiai_write(sock, sendBuffer, sizeof(sendBuffer)));
	
	isConnAlive(kiai_read(sock, receiveBuffer, sizeof(receiveBuffer)));
	_fireStatus f = decodeFire(receiveBuffer);
	
	s->FiresLeft = f.FiresLeft;
	s->PlayersLeft = f.PlayersLeft;
	memcpy(s->Radar, f.Radar, 8);
}

void KIAI_EndTurn() {
	uint8_t sendBuffer[1];
	encodeEndTurn(sendBuffer);
	isConnAlive(kiai_write(sock, sendBuffer, sizeof(sendBuffer)));	
}

void KIAI_Disconnect() {
	sock_close(sock);
	sock_quit();
}
