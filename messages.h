#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include "client.h"

typedef struct {
	uint8_t	X;
	uint8_t	Y;
	uint8_t MovesLeft;
	uint8_t Radar[8];
} _moveStatus;

typedef struct {
	uint8_t PlayersLeft;
	uint8_t FiresLeft;
	uint8_t Radar[8];
} _fireStatus;

void encodePlayerData(uint8_t *output, char *playerName, uint8_t playerVersionMajor, uint8_t playerVersionMinor);
void encodeMove(uint8_t *output, uint8_t d);
void encodeFire(uint8_t *output, uint8_t d);
void encodeEndTurn(uint8_t *output);
KIAI_TurnStatus decodeNewTurn(uint8_t *input);
KIAI_GameConfig decodeGameConfig(uint8_t *input);
_moveStatus decodeMove(uint8_t *input);
_fireStatus decodeFire(uint8_t *input);

#endif
